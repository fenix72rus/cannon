﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LevelManager
{
	public static IEnumerator loseDelay;
	private static Steps currentStep;
	private static Levels[] levels;
	private static LevelSetup levelSetup;
	private static UI uI;
	private static List<Box> boxes = new List<Box>();
	private static List<PoolObject> stands = new List<PoolObject>();
	private static int currentStepNumber, levelNumber, hitsNumber, currentShotsNumber, maxShotsNumber;

	[System.Serializable]
	public struct Levels
	{
		public string name;
		public Steps[] steps;
	}

	[System.Serializable]
	public struct Steps
	{
		public string name;
		public Step step;
		public ObjectPooling ferula;
	}

	public static void Initialize(Levels[] newLevels, LevelSetup _levelSetup, UI _ui)
	{
		levels = newLevels;
		levelSetup = _levelSetup;
		uI = _ui;
	}

	private static void DisableCurrentStep()
    {
		foreach(var stand in stands)
        {
			stand.ReturnToPool();
        }
		stands.Clear();
		foreach(var box in boxes)
        {
			box.poolObject.ReturnToPool();
        }
		boxes.Clear();
    }

	public static void AddHit()
    {
		hitsNumber++;
		if(hitsNumber >= currentStep.step.targetScore)
        {
			NextStep();
		}
    }

	public static void NextStep()
	{
		if (loseDelay != null) levelSetup.StopLose();
		hitsNumber = 0;
		DisableCurrentStep();
		currentStepNumber++;
		if(currentStepNumber >= levels[levelNumber].steps.Length)
        {
			levelNumber++;
			if (levelNumber >= levels.Length) levelNumber = 0;
			LoadLevel(levelNumber);
        }
		else
		{
			currentStep = levels[levelNumber].steps[currentStepNumber];
			SpawnStep();
		}
	}

	public static bool TakeShoot()
	{
		currentShotsNumber++;
		if (currentShotsNumber <= maxShotsNumber)
		{
			if (currentShotsNumber == maxShotsNumber)
			{
				loseDelay = LoseDelay();
				levelSetup.Lose();
			}
			uI.Shot(maxShotsNumber - currentShotsNumber);
			return true;
		}
		else
        {
			return false;
		}
	}

	public static void LoadLevel(int numberOfLevel)
    {
		levelNumber = numberOfLevel;
		uI.SetLevel(numberOfLevel);
		DisableCurrentStep();
		currentStepNumber = 0;
		currentStep = levels[levelNumber].steps[currentStepNumber];
		SpawnStep();
	}

	public static void RestartLevel()
	{
		LoadLevel(levelNumber);
	}

	private static void SpawnStep()
	{
		uI.SetStep(currentStepNumber);
		maxShotsNumber = (currentStepNumber + 1) * 3;
		currentShotsNumber = 0;
		uI.Shot(maxShotsNumber);
		var step = currentStep.step;
		Debug.Log(maxShotsNumber);
		foreach(Transform horizontal in step.HorizontalStands)
        {
			var stand = PoolManager.GetObject("HorizontalStand", horizontal.position, horizontal.rotation).GetComponent<PoolObject>();
			stand.transform.localScale = horizontal.localScale;
			stands.Add(stand);
		}
		foreach(Transform vertical in step.VerticalStands)
        {
			var stand = PoolManager.GetObject("VerticalStend", vertical.position, vertical.rotation).GetComponent<PoolObject>();
			stand.transform.localScale = vertical.localScale;
			stands.Add(stand);
		}
		foreach (Box box in step.boxes)
		{
			Transform boxPrefab = box.transform;
			var _box = PoolManager.GetObject("Box", boxPrefab.position, boxPrefab.rotation).GetComponent<Box>();
			_box.transform.localScale = boxPrefab.localScale;
			_box._collider.enabled = true;
			boxes.Add(_box);
		}
	}

	private static IEnumerator LoseDelay()
    {
		yield return new WaitForSeconds(3f);
		RestartLevel();
	}
}
