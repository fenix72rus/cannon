﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    public PoolObject poolObject;
    public Rigidbody _rigidbody;
    public Collider _collider;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.TryGetComponent(out Ground ground))
        {
            poolObject.ReturnToPool();
            _rigidbody.isKinematic = true;
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.isKinematic = false;
            _collider.enabled = false;
            LevelManager.AddHit();
        }
    }
}
