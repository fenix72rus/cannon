﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private PoolObject poolObject;
    private Rigidbody _rigidbody;
    private TrailRenderer trailRenderer;

    private void Awake()
    {
        poolObject = GetComponent<PoolObject>();
        _rigidbody = GetComponent<Rigidbody>();
        //trailRenderer = GetComponent<TrailRenderer>();
    }

    private void Update()
    {
        if (transform.position.y < 0) Despawn();
    }

    public void Shoot(Vector3 direction, float force)
    {
        _rigidbody.AddForce(direction * force, ForceMode.Force);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.TryGetComponent(out Ground ground))
        {
            Despawn();
        }
        /*if(collision.transform.TryGetComponent<Stand>(out Stand stand))
        {
            Despawn();
        }*/
    }

    private void Despawn()
    {
        //trailRenderer.Clear();
        _rigidbody.velocity = Vector3.zero;
        poolObject.ReturnToPool();
    }
}
