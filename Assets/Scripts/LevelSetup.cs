﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSetup : MonoBehaviour
{
	private LevelManager.Steps[] steps;
	[SerializeField] private UI ui;
	[SerializeField] private LevelManager.Levels[] levels;

	public void Lose()
    {
		StartCoroutine(LevelManager.loseDelay);
    }

	public void StopLose()
    {
		if (LevelManager.loseDelay != null) 
			StopCoroutine(LevelManager.loseDelay);
    }
	
	void Awake()
	{
		Initialize();
	}

    private void Start()
	{
		LevelManager.LoadLevel(0);
	}

    void Initialize()
	{
		LevelManager.Initialize(levels, this, ui); 
	}
}
