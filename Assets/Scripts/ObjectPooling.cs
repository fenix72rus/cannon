﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling
{
	List<PoolObject> bullets;
	Transform objectsParent;

	public void Initialize(int count, PoolObject sample, Transform objects_parent)
	{
		bullets = new List<PoolObject>();
		objectsParent = objects_parent;
		for (int i = 0; i < count; i++)
		{
			AddObject(sample, objects_parent);
		}
	}

	public PoolObject GetObject()
	{
		for (int i = 0; i < bullets.Count; i++)
		{
			if (bullets[i].gameObject.activeInHierarchy == false)
			{
				return bullets[i];
			}
		}
		AddObject(bullets[0], objectsParent);
		return bullets[bullets.Count - 1];
	}

	private void AddObject(PoolObject bullet, Transform objects_parent)
	{
		GameObject temp = GameObject.Instantiate(bullet.gameObject);
		temp.name = bullet.name;
		temp.transform.SetParent(objects_parent);
		bullets.Add(temp.GetComponent<PoolObject>());
		temp.SetActive(false);
	}
}
