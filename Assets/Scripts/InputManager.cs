﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private Cannon cannon;

    private void Update()
    {
        var touches = Input.touches;
        if(touches.Length > 0)
        {
            foreach(var touch in touches)
            {
                if(touch.phase == TouchPhase.Began)
                {
                    cannon.Shoot(Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 6)));
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                cannon.Shoot(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 6)));
            }
        }
    }
}
