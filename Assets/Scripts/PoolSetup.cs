﻿using UnityEngine;
using System.Collections;

public class PoolSetup : MonoBehaviour
{//обертка для управления статическим классом PoolManager

	[SerializeField] private PoolManager.PoolPart[] pools; //структуры, где пользователь задает префаб для использования в пуле и инициализируемое количество 

	void OnValidate()
	{
		for (int i = 0; i < pools.Length; i++)
		{
			pools[i].name = pools[i].prefab.name; //присваиваем имена заранее, до инициализации
		}
	}

	void Awake()
	{
		Initialize();
	}

	void Initialize()
	{
		PoolManager.Initialize(pools); //инициализируем менеджер пулов
	}

}
