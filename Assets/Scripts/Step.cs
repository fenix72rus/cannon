﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Step : MonoBehaviour
{
    public Box[] boxes;
    public Transform[] HorizontalStands;
    public Transform[] VerticalStands;
    public int targetScore;
}
