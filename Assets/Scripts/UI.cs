﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public TextMeshProUGUI currentShotText, currentLevelText, nextLevelText;
    public Image[] stepsImages;

    public void Shot(int shotNumber)
    {
        currentShotText.text = shotNumber.ToString();
    }

    public void SetStep(int stepNumber)
    {
        stepsImages[stepNumber].color = new Color32(255, 255, 0, 255);
    }

    public void SetLevel(int levelNumber)
    {
        currentLevelText.text = (levelNumber + 1).ToString();
        nextLevelText.text = (levelNumber + 2).ToString();
        foreach(var step in stepsImages)
        {
            step.color = new Color32(255, 175, 0, 255);
        }
    }
}
