﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [SerializeField] private float shootForce;
    [SerializeField] private Transform muzzle;


    public void Shoot(Vector3 direction)
    {
        if(LevelManager.TakeShoot())
        {
            Bullet bullet = PoolManager.GetObject("Bullet", muzzle.position, Quaternion.identity).GetComponent<Bullet>();
            Vector3 point = muzzle.position + direction.normalized * 6;
            direction = (point - bullet.transform.position).normalized;
            bullet.Shoot(direction, shootForce);
        }
    }


}
